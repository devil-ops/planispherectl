/*
Package main is the main executable
*/
package main

import "gitlab.oit.duke.edu/devil-ops/planispherectl/planispherectl/cmd"

func main() {
	cmd.Execute()
}
