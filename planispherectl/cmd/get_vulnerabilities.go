package cmd

import (
	"log/slog"

	goutbra "github.com/drewstinnett/gout-cobra"
	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/planisphere-sdk/planisphere"
)

// getVulnerabilitiesCmd represents the getVulnerabilities command
var getVulnerabilitiesCmd = &cobra.Command{
	Use:     "vulnerabilities",
	Short:   "Get vulnerability information on Devices in Planisphere",
	Aliases: []string{"vulnerability", "vuln", "vulns", "v"},
	RunE: func(cmd *cobra.Command, _ []string) error {
		if err := goutbra.Cmd(getCmd); err != nil {
			return err
		}

		p := client.NewReportParams(
			planisphere.WithVulnerabilityData(),
			planisphere.WithActiveSince(mustGetCmd[int](*cmd, "active-since")),
			planisphere.WithCmd(*cmd),
		)
		p.VulnerabilitySeverities = severitiesWithCmd(*cmd)

		devices, _, err := client.Report.Generate(ctx, p)
		if err != nil {
			return err
		}

		// Populate out a big ol' list with all the vulns
		var allVulns []vulnHolder
		var hostCount int
		for _, device := range *devices {
			if len(*device.VulnerabilityDetections) > 0 {
				hostCount++
				for _, vuln := range *device.VulnerabilityDetections {
					if vuln != nil {
						allVulns = append(allVulns, vulnHolder{
							Vulnerability: *vuln,
							Device:        device.Summary(),
						})
					}
				}
			}
		}
		gout.MustPrint(allVulns)

		slog.Info("returned items", "vulns", len(allVulns), "hosts", hostCount)
		return nil
	},
}

func init() {
	getCmd.AddCommand(getVulnerabilitiesCmd)
	getVulnerabilitiesCmd.PersistentFlags().IntP("active-since", "c", 1, "Only return devices 'active since' N days ago")
	bindHostFilters(getVulnerabilitiesCmd)
	bindVulnFilters(getVulnerabilitiesCmd)
}

type vulnHolder struct {
	Vulnerability planisphere.Vulnerability `csv:"vuln_,inline"`
	Device        planisphere.DeviceSummary `csv:"device_,inline"`
}
