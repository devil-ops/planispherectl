package cmd

import (
	"fmt"
	"log/slog"
	"os"
	"reflect"
	"time"

	"github.com/charmbracelet/glamour"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/planisphere-sdk/planisphere"
)

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}

/*
func containsString(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
*/

func mustGetCmd[T []int | int | []string | string | bool | time.Duration](cmd cobra.Command, s string) T {
	switch any(new(T)).(type) {
	case *int:
		item, err := cmd.Flags().GetInt(s)
		panicIfErr(err)
		return any(item).(T)
	case *string:
		item, err := cmd.Flags().GetString(s)
		panicIfErr(err)
		return any(item).(T)
	case *[]string:
		item, err := cmd.Flags().GetStringArray(s)
		panicIfErr(err)
		return any(item).(T)
	case *bool:
		item, err := cmd.Flags().GetBool(s)
		panicIfErr(err)
		return any(item).(T)
	case *[]int:
		item, err := cmd.Flags().GetIntSlice(s)
		panicIfErr(err)
		return any(item).(T)
	case *time.Time:
		item, err := cmd.Flags().GetDuration(s)
		panicIfErr(err)
		return any(item).(T)
	default:
		panic(fmt.Sprintf("unexpected use of mustGetCmd: %v", reflect.TypeOf(s)))
	}
}

func renderMarkdown(s string) {
	// Render
	rend, err := glamour.NewTermRenderer(
		glamour.WithAutoStyle(),
		glamour.WithWordWrap(140),
	)
	panicIfErr(err)
	out, err := rend.Render(s)
	panicIfErr(err)
	fmt.Print(out)
}

func severitiesWithCmd(cmd cobra.Command) *planisphere.VulnerabilityCriticalities {
	vulnSeverities := mustGetCmd[[]string](cmd, "vulnerability-severity")
	if len(vulnSeverities) > 0 {
		sevs := planisphere.VulnerabilityCriticalities{}
		for _, severityString := range vulnSeverities {
			severityInt, verr := planisphere.GetVulnerabilityCriticality(severityString)
			if verr != nil {
				slog.Error("invalid severity", "severity", severityString, "possible", planisphere.VulnerabilityCriticalityNames())
				os.Exit(2)
				// log.Fatal().Err(verr).Str("severity", severityString).Strs("possible", planisphere.VulnerabilityCriticalityNames()).Msg("Invalid severity")
			}
			sevs = append(sevs, severityInt)
		}
		return &sevs
	}
	return nil
}

/*
func reportParamsWithCmd(cmd cobra.Command) *planisphere.ReportParams {
	p := client.NewReportParams()
	// Filters
	if cmd.Flags().Changed("os-responsibility") {
		p.ResponsibilityType = "os"
	}
	if cmd.Flags().Changed("app-responsibility") {
		p.ResponsibilityType = "app"
	}
	if cmd.Flags().Changed("actionable") {
		actionable := mustGetCmd[bool](cmd, "actionable")
		p.Actionable = &actionable
	}

	p.HostnameFilters = mustGetCmd[[]string](cmd, "exclude-host")
	p.VulnFilters = mustGetCmd[[]string](cmd, "exclude-vuln")

	return p
}
*/

func bindHostFilters(cmd *cobra.Command) {
	cmd.PersistentFlags().String("match-hostnames", "", `Filter based off of hostnames. This does a partial string match, so "web-" will match both "web-host-01.example.com" and "internal-web-host-01.example.com"`)
	cmd.PersistentFlags().StringArray("exclude-host", []string{}, "Exclude hosts matching this string. This just matches partial strings, nothing fancy")
}

func bindVulnFilters(cmd *cobra.Command) {
	cmd.PersistentFlags().StringArray("exclude-vuln", []string{}, "Exclude vulns matching this string. This just matches partial strings, nothing fancy")
	cmd.PersistentFlags().StringArrayP("vulnerability-severity", "s", []string{}, `Limit to devices with Vulnerability severity. Specify multiple time for
multiple severities. Possible values are: Critical, High, Low`)
	cmd.PersistentFlags().String("responsibility", "any", fmt.Sprintf("Responsible party for the vulnerability. %v", planisphere.ResponsibilityHelp))
	cmd.PersistentFlags().String("actionability", "actionable", fmt.Sprintf("Only return vulns with an actionability type. %v", planisphere.ActionabilityHelp))
	cmd.PersistentFlags().String("risk", "unaccepted", fmt.Sprintf("Only return vulns with an accepted risk type. %v", planisphere.RiskHelp))
}

// precannedReports are some hard coded report params
var precannedReports = map[string]string{
	"ssi-missing-crowdstrike": `active_since=1&c[]=active_since&c[]=names&c[]=quarantine_risks&c[]=endpoint_mgmt_status&c[]=quarantined_at&c[]=usage_type&c[]=user&c[]=support_group_id&c[]=department_key&c[]=device_type&c[]=manufacturer&c[]=os_family&c[]=serial&c[]=mac_addresses&c[]=data_sources&c[]=custom_field_monitored&data_sources[]=!30&dir=desc&per_page=100&quarantined_at=0&sort=active_since&support_group_id[]=41&support_group_id[]=49`,
	"ssi-quarantined":         `active_since=30&c[]=active_since&c[]=names&c[]=quarantine_risks&c[]=quarantined_at&c[]=usage_type&c[]=user&c[]=support_group_id&c[]=department_key&c[]=device_type&c[]=manufacturer&c[]=os_family&c[]=serial&c[]=mac_addresses&c[]=custom_field_monitored&dir=desc&quarantined_at=1&sort=active_since&support_group_id[]=41&support_group_id[]=49`,
	"vcm-under-ssi":           `active_since=1&c[]=active_since&c[]=names&c[]=quarantine_risks&c[]=quarantined_at&c[]=usage_type&c[]=user&c[]=support_group_id&c[]=department_key&c[]=device_type&c[]=manufacturer&c[]=os_family&c[]=serial&c[]=mac_addresses&c[]=custom_field_monitored&dir=desc&names=vm.duke.edu&sort=active_since&support_group_id[]=41&support_group_id[]=49`,
}

// listPrecannedReports lists out the said reports
func listPrecannedReports() []string {
	keys := make([]string, len(precannedReports))

	i := 0
	for k := range precannedReports {
		keys[i] = k
		i++
	}
	return keys
}
