package cmd

import (
	"log/slog"

	goutbra "github.com/drewstinnett/gout-cobra"
	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
)

// getSubnetsCmd represents the getSubnets command
var getSubnetsCmd = &cobra.Command{
	Use:     "subnets",
	Short:   "Get information on Subnets in Planisphere",
	Aliases: []string{"sg", "subnet"},
	Example: `$ planispherectl get subnet`,
	RunE:    getSubnetsRun,
}

func getSubnetsRun(_ *cobra.Command, _ []string) error {
	if err := goutbra.Cmd(getCmd); err != nil {
		return err
	}
	r, _, err := client.Subnet.List(ctx)
	if err != nil {
		return err
	}
	gout.MustPrint(r)
	slog.Info("returned items", "items", len(*r))
	return nil
}

func init() {
	getCmd.AddCommand(getSubnetsCmd)
}
