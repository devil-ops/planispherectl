package cmd

import (
	"context"
	"fmt"
	"sort"
	"strings"

	"github.com/charmbracelet/log"
	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/planisphere-sdk/planisphere"
)

// todoCmd represents the todo command
var todoCmd = &cobra.Command{
	Use:   "todo",
	Short: "Query planisphere for devices you support that need some work",
	RunE: func(cmd *cobra.Command, _ []string) error {
		ctx := context.Background()
		// baseParams := reportParamsWithCmd(*cmd)
		detailed := mustGetCmd[bool](*cmd, "detailed")
		baseParams := client.NewReportParams(planisphere.WithCmd(*cmd))
		// log.Fatal().Interface("b", baseParams).Send()
		report := strings.Builder{}
		report.WriteString(todoHelp)
		// Hosts without CrowdStrike
		log.Debug("Lookup up hosts that need crowdstrike")
		p := baseParams
		p.QuarantineRisks = []string{"crowd_strike"}
		r, _, err := client.Report.Generate(ctx, p)
		if err != nil {
			return err
		}

		if len(*r) > 0 {
			report.WriteString(fmt.Sprintf("\n## Found %v hosts without CrowdStrike reporting in:\n", (len(*r))))
			sort.Sort(*r)
			for _, item := range *r {
				report.WriteString(fmt.Sprintf("- [%v](%v)\n", item.PrimaryName, item.URL))
			}
		} else {
			report.WriteString("\n## Wow, crowdstrike is on all your appropriate hosts! Give yourself a nice pat on the back 🎉")
		}

		// Hosts with a Critical Vulnerability
		log.Debug("Lookup up hosts that have quarantine worthy vulnerabilities")
		p = baseParams
		p.QuarantineRisks = []string{"vulnerability"}
		p.IncludeVulnerablityData = true
		r, _, err = client.Report.Generate(ctx, p)
		if err != nil {
			return err
		}
		if len(*r) > 0 {
			vulnMapi := vulnMap{}
			sort.Sort(*r)
			report.WriteString(fmt.Sprintf("\n## Found %v hosts with quarantine worthy vulnerabilities:\n", (len(*r))))
			for _, item := range *r {
				report.WriteString(fmt.Sprintf("- [%v](%v/vulnerabilities)\n", item.PrimaryName, item.URL))
				if item.VulnerabilityDetections != nil {
					for _, v := range *item.VulnerabilityDetections {
						if v != nil {
							vulnMapi[v.Description] = append(vulnMapi[v.Description], item)
						}
					}
				}
			}

			// Frequently seen Vulnerabilities
			// Let's order this map by host count
			orderedItems := make([]vulnSummary, len(vulnMapi))
			i := 0
			for desc, devices := range vulnMapi {
				orderedItems[i] = vulnSummary{
					Description: desc,
					Devices:     devices,
				}
				i++
			}
			sort.Slice(orderedItems, func(i, j int) bool {
				return len(orderedItems[i].Devices) > len(orderedItems[j].Devices)
			})

			topn := 5
			if detailed {
				topn = len(orderedItems)
			}
			if len(orderedItems) < topn {
				topn = len(orderedItems)
			}
			report.WriteString(fmt.Sprintf("\n## Top %v Most Frequent Vulnerabilities (out of %v):\n", topn, len(orderedItems)))

			for _, vulnS := range orderedItems[0:topn] {
				report.WriteString(fmt.Sprintf("- %v (%v hosts)\n", vulnS.Description, len(vulnS.Devices)))
				maxDevices := 5
				if detailed {
					maxDevices = len(vulnS.Devices)
				}
				if maxDevices > len(vulnS.Devices) {
					maxDevices = len(vulnS.Devices)
				}

				sort.Sort(vulnS.Devices)
				for _, d := range vulnS.Devices[0:maxDevices] {
					report.WriteString(fmt.Sprintf("  - [%v](%v/vulnerabilities)\n", d.PrimaryName, d.URL))
				}
				remainder := len(vulnS.Devices) - maxDevices
				if remainder > 0 {
					report.WriteString(fmt.Sprintf("  - …[%v more hosts not shown, use `--detailed` to see them all]…\n", remainder))
				}
			}
		} else {
			report.WriteString("\n## Wow, no nasty vulns on your hosts! Give yourself a nice pat on the back 🎉")
		}

		renderMarkdown(report.String())
		return nil
	},
}

func init() {
	rootCmd.AddCommand(todoCmd)
	todoCmd.Flags().Bool("detailed", false, "Show all vulnerablities instead of just the top 5")
	bindHostFilters(todoCmd)
	bindVulnFilters(todoCmd)
}

type vulnSummary struct {
	Description string
	Devices     planisphere.DeviceList
}

type vulnMap map[string][]planisphere.Device
