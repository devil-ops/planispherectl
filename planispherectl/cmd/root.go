/*
Package cmd holds all the CLI bits
*/
package cmd

import (
	"context"
	"errors"
	"log/slog"
	"os"
	"time"

	// "github.com/rs/zerolog/log"
	"github.com/charmbracelet/log"

	goutbra "github.com/drewstinnett/gout-cobra"
	"github.com/spf13/cobra"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
	"gitlab.oit.duke.edu/devil-ops/planisphere-sdk/planisphere"
)

var cfgFile string

var (
	version = "dev"
	ctx     context.Context
	// Verbose will enable much more verbose logging where applicable
	Verbose bool
)

var client *planisphere.Client

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:     "planispherectl",
	Short:   "Get Planisphere stuff from the API on the CLI",
	Long:    `Interact with Planisphere y'all`,
	Version: version,
	PersistentPreRunE: func(cmd *cobra.Command, _ []string) error {
		username := viper.GetString("username")
		token := viper.GetString("token")
		ctx = context.Background()
		if username == "" {
			return errors.New("set username please")
		}
		if token == "" {
			return errors.New("set token please")
		}
		// client = planisphere.NewClient(username, token, nil)
		opts := []func(*planisphere.Client){
			planisphere.WithUsername(username),
			planisphere.WithToken(token),
		}

		// Set the scope
		if mustGetCmd[bool](*cmd, "all-support-groups") {
			opts = append(opts, planisphere.WithSupportGroupScope(planisphere.SGScopeAll))
		} else if len(mustGetCmd[[]string](*cmd, "support-group")) > 0 {
			opts = append(opts, planisphere.WithSupportGroupScope(planisphere.SGScopeSelected))
		}

		client = planisphere.MustNew(opts...)

		// Add selected bits if needed
		if len(mustGetCmd[[]string](*cmd, "support-group")) > 0 {
			for _, sgk := range mustGetCmd[[]string](*cmd, "support-group") {
				sg, _, err := client.SupportGroup.Get(context.Background(), sgk)
				if err != nil {
					return err
				}
				client.AddScopedSupportGroup(*sg)
			}
		}
		return nil
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.SetVersionTemplate("{{.Version}}\n")
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.planispherectl.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.PersistentFlags().BoolP("all-support-groups", "a", false, "Include devices for all support groups")
	rootCmd.PersistentFlags().StringArray("support-group", []string{}, "Support groups to include. This can be one of: grouper path, name, or numerical id of the group. Can be specified multiple times.")
	rootCmd.MarkFlagsMutuallyExclusive("all-support-groups", "support-group")

	rootCmd.PersistentFlags().BoolVarP(&Verbose, "verbose", "v", false, "Enable verbose output")

	if err := goutbra.Bind(getCmd); err != nil {
		panic(err)
	}

	if err := goutbra.Bind(reportCmd); err != nil {
		panic(err)
	}

	initLogging()
}

func initLogging() {
	// cobra.OnInitialize(initConfig)
	lopts := log.Options{
		TimeFormat: time.StampMilli,
		Prefix:     "planispherectl 🌎 ",
	}
	if Verbose {
		lopts.Level = log.DebugLevel
	}
	slog.SetDefault(slog.New(log.NewWithOptions(
		os.Stderr,
		lopts,
	)))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".planispherectl" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".planispherectl")
	}

	viper.AutomaticEnv() // read in environment variables that match
	viper.SetEnvPrefix("planispherectl")

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		log.Debug("using config file", "file", viper.ConfigFileUsed())
	}
}
