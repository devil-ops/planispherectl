package cmd

import (
	"log/slog"

	goutbra "github.com/drewstinnett/gout-cobra"
	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
)

// getSupportGroupsCmd represents the getSupportGroups command
var getSupportGroupsCmd = &cobra.Command{
	Use:     "supportgroups",
	Short:   "Get information on SupportGroups in Planisphere",
	Aliases: []string{"sg", "supportgroup"},
	Example: `$ planispherectl get supportgroup`,
	RunE:    getSupportGroupsRun,
}

func init() {
	getCmd.AddCommand(getSupportGroupsCmd)
}

func getSupportGroupsRun(_ *cobra.Command, _ []string) error {
	if err := goutbra.Cmd(getCmd); err != nil {
		return err
	}
	r, _, err := client.SupportGroup.List(ctx)
	if err != nil {
		return err
	}

	gout.MustPrint(r)
	slog.Info("returned items", "items", len(*r))
	return nil
}
