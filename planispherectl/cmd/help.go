package cmd

var todoHelp = `# PlanisphereCTL TODO!

Thanks for checking on things that needs some attention! Below are the things we
think you'll be most interested in.

If you are seeing hosts that you don't think belong to you, make sure the
support group in Planisphere is correct.

Full all this info and more, check out [Planisphere](https://planisphere.oit.duke.edu)

`
