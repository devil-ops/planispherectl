package cmd

import (
	"fmt"
	"net/url"
	"strings"

	goutbra "github.com/drewstinnett/gout-cobra"
	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
)

// reportCmd represents the report command
var reportCmd = &cobra.Command{
	Use:     "report",
	Short:   "Do a report on devices in Planisphere",
	Aliases: []string{"api"},
	Args:    cobra.ExactArgs(1),
	Example: `You can use this command to take a dashboard view from Planisphere, and parse it
on the command line.

Do a copy from the 'API Equavalent' link at the bottom any custom dashboard, and
run it with:

Simple YAML Report:

$ planispherectl report $URL

Use a custom output format:

$ planispherectl report $URL \
  --format gotemplate \
  --format-template '{{ range . }}{{printf "Hello, I am %s and my security score is %f\\n" .PrimaryName .SecurityScore}}{{ end }}'

NOTE: If you would like to see all the available options in the template above,
run the report with '--format=plain'. That will show you all of the possible
keys for the template to use.`,
	Long: `You can specify several popular output formats using the --format option.
JSON/YAML (and any superset of that) should work fine. Things like XML/CSV
aren't quite as seamless, so your results may vary with those. If you run in to
issues with any of the formats, let us know and we'll try to get it working!



`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if err := goutbra.Cmd(cmd); err != nil {
			return err
		}

		var reportContents string
		switch {
		case strings.HasPrefix(args[0], "http"):
			URL, err := url.ParseRequestURI(args[0])
			if err != nil {
				return err
			}
			decodedValue, err := url.QueryUnescape(URL.RawQuery)
			if err != nil {
				return err
			}
			reportContents = decodedValue
		case strings.Contains(args[0], "?"):
			reportContents = args[0]
		default:
			reportContents = precannedReports[args[0]]
		}

		if reportContents == "" {
			return fmt.Errorf("that is not a valid canned report, please use one of: %v", listPrecannedReports())
		}

		r, _, err := client.Report.Get(ctx, reportContents)
		if err != nil {
			return err
		}

		return gout.Print(r)
	},
}

func init() {
	rootCmd.AddCommand(reportCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// reportCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// reportCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
