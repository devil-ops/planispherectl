package cmd

import (
	"context"
	"log/slog"

	goutbra "github.com/drewstinnett/gout-cobra"
	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
)

// getDevicesCmd represents the getDevices command
var getDevicesCmd = &cobra.Command{
	Use:   "devices",
	Short: "Get information on Devices in Planisphere",
	Long: `Used for pulling in devices using the /devices/reports API.  By default, this
will look at the current users support groups, and limit results to those
groups.`,

	Aliases: []string{"device", "d"},
	Example: `$ planispherectl get device
$ planispherectl get device 123`,
	RunE: func(cmd *cobra.Command, _ []string) error {
		p := client.NewReportParams()
		p.IncludeVulnerablityData = true

		if err := goutbra.Cmd(getCmd); err != nil {
			return err
		}

		// Handle filter by qurantine risk here
		quarantineRisks, _ := cmd.Flags().GetStringArray("quarantine-risk")
		if len(quarantineRisks) > 0 {
			p.QuarantineRisks = quarantineRisks
		}

		// Handle VRFs
		vrfs, _ := cmd.Flags().GetStringArray("vrf")
		if len(vrfs) > 0 {
			p.VRFs = vrfs
		}

		// How far are we going back?
		p.ActiveSince = mustGetCmd[int](*cmd, "active-since")

		name := mustGetCmd[string](*cmd, "name")
		if name != "" {
			p.Names = name
		}

		r, _, err := client.Report.Generate(context.Background(), p)
		if err != nil {
			return err
		}

		gout.MustPrint(r)
		slog.Info("returned items", "items", len(*r))
		return nil
	},
}

func init() {
	getCmd.AddCommand(getDevicesCmd)
	getDevicesCmd.PersistentFlags().IntP("active-since", "c", 1, "Only return devices 'active since' N days ago")
	getDevicesCmd.PersistentFlags().StringP("name", "n", "", "Filter results to only include this name match")
	// getDevicesCmd.PersistentFlags().StringArrayP("vulnerability-severity", "s", []string{}, `Limit to devices with Vulnerability severity. Specify multiple time for
	// multiple severities. Possible values are: Critical, High, Low`)
	getDevicesCmd.PersistentFlags().StringArrayP("quarantine-risk", "q", []string{}, `Limit to devices with a risk of being qurantined. Specify multiple times for
multiple reasons Possible values are: unsupported_os, vulnerability,
endpoint_management `)
	getDevicesCmd.PersistentFlags().StringArrayP("vrf", "r", []string{}, `Limit to devices within a VRF. Specify multiple times for
multiple vrfs`)

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getDevicesCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	// getDevicesCmd.Flags().BoolP("include-all", "i", false, "Include devices for all support groups, not just mine")
}
