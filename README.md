# PlanisphereCTL

PlanisphereCTL is used for interacting with the Planisphere API. By default,
most commands are going to only return hosts that belong to a Service Group you
are a member of. This behavior can be changed by specifying the `--support-group
"some-support-group"` flag. This flag can be used multiple times for multiple
support groups. You may also use `--all-support-groups`, which will remove the
support group filting.

For full usage, check out `planispherectl --help`.

## Installation

Binaries can be downloaded from the
[release](https://gitlab.oit.duke.edu/devil-ops/planispherectl/-/releases) page,
or you can use the [devil-ops
packaging](https://gitlab.oit.duke.edu/devil-ops/installing-devil-ops-packages)
to install the package.

## Setup

Make sure you have an API credential. You can generate one
[here](https://planisphere.oit.duke.edu/api_info)

Credentials can be stored in one or a combination of ENV variables or the config file `~/.planispherectl.yaml` file.

File Only Example, for things that need full automation:

```shell
$ cat ~/.planispherectl.yaml
---
username: <netid>
token: <api-token>
```

ENV Only Example:

```shell
export PLANISPHERECTL_USERNAME=mynetid
export PLANISPHERECTL_TOKEN=mytoken
```

🔥 Max Security Example:

```shell
$ cat ~/.planispherectl.yaml
---
username: <netid>
$ export PLANISPHERECTL_TOKEN=mytoken
```

another high security example using `op read`, more examples [@developer.1password.com](https://developer.1password.com/docs/cli/secrets-scripts/).

```shell
# fish shell function
function planispherectl
        set -x PLANISPHERECTL_USERNAME (op read op://Private/planisphere-api/username)
        set -x PLANISPHERECTL_TOKEN (op read op://Private/planisphere-api/token)
        /usr/bin/env planispherectl $argv
end
```

1Password command-line example:

If you have your API token in 1Password, you can use their [`op` command-line program](https://1password.com/downloads/command-line/) to call the token from 1Password using your fingerprint if you have a reader, but your regular 1Password login if you don't.
Then you can set your token:

```shell
fish-shell: 
export PLANISPHERECTL_TOKEN=(op item get Planisphere\ API --fields password)
or bash:
export PLANISPHERECTL_TOKEN=$(op item get Planisphere\ API --fields password)
```

In fish-shell you can make it into a function, then just execute the function on your command-line:

```shell
function planisphere-api
  export PLANISPHERECTL_USERNAME=<netid>
  export PLANISPHERECTL_TOKEN=(op item get Planisphere\ API --fields password)
end
```

## Usage

### Get Planisphere items in a parsable format, such as YAML or JSON

Get all devices supported by your support groups

```shell
$ planispherectl get devices --format json
...
```

Get a list of support groups in yaml

```shell
$ planispherectl get supportgroups --format yaml
...
```

Get a list of actionable vulnerabilities

```shell
$ planispherectl get vulnerabilities
...
```

Get a list of actionable vulnerabilities that are classified as `os responsibility`

```shell
$ planispherectl get vulnerabilities --responsibility=os
...
```

Use `planispherectl --help` for full options

### Custom Output Format

Output is in YAML by default, however you can specify a number of additional
formats using the `--format` argument. The JSON style formats will work best (JSON, YAML, TOML, etc). CSV mostly works, however some data is lost during the conversion. If you have a need for data in CSV format that isn't working, just pop a request in using the [New Issue link](https://gitlab.oit.duke.edu/devil-ops/planispherectl/-/issues/new)

If you use the `gotemplate` format, you can also specify `--format-template` in
the golang template format.  Example:

```shell
$ planispherectl/ get devices --name foo --format gotemplate --format-template '{{ range . }}{{printf "Hello, I am %s and my security score is %f\\n" .PrimaryName .SecurityScore}}{{ end }}'
Hello, I am foo-01.example.edu and my security score is 100.000000
Hello, I am web-foo-01.example.edu and my security score is 90.000000
```

### Run Reports

Got a super sweet report you use in the Planisphere Web interface?  Get that
thing from the command line!

On the website, check out the "API Equivalent" link at the bottom of the page.
Just paste those arguments in to the report command like so:

```shell
$ planispherectl report $URL_FROM_ABOVE
...
```

### Find things that need attention

Using `planisphere todo`, you can get a summary of things that need attention,
like devices missing crowdstrike, hosts with vulnerabilities, and the top
vulnerabilities across hosts.

*This code is freely available for non-commercial use and is provided as-is with no warranty.*
